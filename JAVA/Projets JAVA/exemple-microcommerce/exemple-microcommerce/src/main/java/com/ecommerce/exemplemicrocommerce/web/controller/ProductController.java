package com.ecommerce.exemplemicrocommerce.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ecommerce.exemplemicrocommerce.model.Product;

@Controller
@ResponseBody //@RestController => combinaison de ces 2 annotation
public class ProductController {

	@RequestMapping(value="/Produits", method=RequestMethod.GET)
	public String listeProduits() {
		
		return "Un exemple de produit";
	}
	
	@GetMapping(value="/Produits/{id}") // @GetMapping(value="/Produits/{id}") = @RequestMapping(value="/Produits/{id}", method=RequestMethod.GET) (raccourci d'écriture)
		public Product afficherUnProduit(@PathVariable int id) {
		
		// return "Vous avez demandé le produit avec l'id " + id => pour test
		Product product = new Product(id,new String ("Aspirateur"), 100);
		return product;
	}

}

