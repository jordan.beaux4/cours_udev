package IPI.monument.monument;

import IPI.monument.Utils;
import IPI.monument.utilisateur.Utilisateur;
import IPI.monument.utilisateur.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller //@Controller indique que la classe est un controller, qui va permettre l'échange avec DB
@RequestMapping("/monument") //@RequestMapping indique le chemin URL a taper pour accdéer au service
public class MonumentController {

    @Autowired //@Autowired implémente l'interface du nom qui suit dans la classe désignée (et on le renomme derrière ici, ex : pour faire référence à MonumentRepository, ici on utilisera repo) 
    MonumentRepository repo;

    @Autowired
    UtilisateurRepository users;

    //METHODE GET
    @RequestMapping("/{id}")
    @ResponseBody //@ResponseBody indique que ce qui suit (donc l'élément à récupérer) est la réponse (et pas une page/template)
    public Monument getMonument(@PathVariable("id") Integer id){ //(@PathVariable("élément") TypeElement élément) => change dynamiquement l'url (si on tape l'id 1, on lance la méthode avec 1 en paramètre et on l'affiche)
        Monument result = repo.findById(id).get();
        return result;
    }

    //METHODE GET ALL
    @RequestMapping("/all")
    @ResponseBody
    public Iterable<Monument> getMonuments(@RequestParam(value="categorie",required = false) String categorie){
    	//@RequestParam pour récupérer le paramètre d'URL et l'associer à l'argument de la méthode.
    	//@RequestParam est utilisée pour lier les paramètres de requête à un paramètre de méthode dans votre contrôleur.
        if (categorie==null)
        return repo.findAll();
        else {
            return repo.findByCategorie(categorie);
        }
    }


    //METHODE POST
    @RequestMapping(value="",method = RequestMethod.POST)
    @ResponseBody
    public Monument postMonument(@RequestBody Monument m,
                                 @RequestHeader("login") String login, //@RequestHeader vérifie l'en-tête avec le nom spécifié dans l'annotation et lie sa valeur au paramètre de méthode de gestionnaire
                                 @RequestHeader("password") String password){
        if (users.authentifierAdmin(login,Utils.encrypt(password))!=null){
            Monument result = repo.save(m);
            return result;
        }
        return null;
    }

    //METHODE PUT
    @RequestMapping(value="",method = RequestMethod.PUT)
    @ResponseBody
    public Monument putMonument(@RequestBody Monument m,
                                @RequestHeader("login") String login,
                                @RequestHeader("password") String password){
        if (users.authentifierAdmin(login,Utils.encrypt(password))!=null) {
            Monument toUpdate = repo.findById(m.getId()).get();
            toUpdate.setNom(m.getNom());
            toUpdate.setDescription(m.getDescription());
            toUpdate.setPts(m.getPts());
            toUpdate.setUrlAudio(m.getUrlAudio());
            toUpdate.setUrlImage(m.getUrlImage());
            repo.save(toUpdate);
            return toUpdate;
        }
        return null;
    }

    //METHODE DELETE
    @RequestMapping(value="/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Monument deleteMonument(@PathVariable("id") Integer id,
                                   @RequestHeader("login") String login,
                                   @RequestHeader("password") String password){
        if (users.authentifierAdmin(login, Utils.encrypt(password))!=null) {
            Monument m = repo.findById(id).get();
            repo.delete(m);
            return m;
        }
        return null;
    }


}
