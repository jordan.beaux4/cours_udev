package IPI.monument.utilisateur;

import IPI.monument.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Controller
@RequestMapping("/utilisateur")
public class UtilisateurController {

    @Autowired
    UtilisateurRepository repo;

    @RequestMapping("/all")
    @ResponseBody
    public Iterable<Utilisateur> donneUtilisateurs(){

        ArrayList<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();
        for (Utilisateur u : repo.findAll()){
            Utilisateur u2 = new Utilisateur();
            u2.setLogin(u.getLogin());
            u2.setMail(u.getMail());
            u2.setNom(u.getNom());
            u2.setPrenom(u.getPrenom());
            utilisateurs.add(u2);
        }

        return utilisateurs;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public Utilisateur creer(@RequestBody Utilisateur u){
        u.setPassword(Utils.encrypt(u.getPassword()));
        return repo.save(u);
    }

    @RequestMapping(value = "/auth")
    @ResponseBody
    public Utilisateur authentifier(@RequestHeader("login") String login,
                                    @RequestHeader("password") String password){
        Utilisateur u = repo.findById(login).get();
        if(u.getPassword().equals(Utils.encrypt(password))){
            return u;
        }
        return null;
    }

    @RequestMapping(value = "{login}", method = RequestMethod.DELETE)
    @ResponseBody
    public void supprimer(@PathVariable("login") String login){
        repo.deleteById(login);
    }
}
