package IPI.monument;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

@SpringBootApplication
public class MonumentApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonumentApplication.class, args);
	}


}
