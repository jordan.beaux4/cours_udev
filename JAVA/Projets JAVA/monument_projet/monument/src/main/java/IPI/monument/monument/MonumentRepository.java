package IPI.monument.monument;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Transactional // Nécessaire pour indiquer qu'il y aura échange avec la DB
@Repository // Indique que la classe concernée accède directement à la DB
public interface MonumentRepository extends CrudRepository<Monument,Integer> { //extends CrudRepository<typeEntité,type de l'ID de l'entité> => fournit les méthodes communes Create Read Update Delete 

    @Query("SELECT m FROM Monument m WHERE categorie=?1")
    //In order to define SQL to execute for a Spring Data repository method, 
    //we can annotate the method with the @Query(requête SQL).
    Iterable<Monument> findByCategorie(String categorie);
}
