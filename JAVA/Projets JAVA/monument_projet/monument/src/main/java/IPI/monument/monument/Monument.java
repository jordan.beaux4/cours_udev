package IPI.monument.monument;

import javax.persistence.*;
import java.util.List;

@Entity //@Entity nous indique que cette classe est une classe persistante (stocké dans DB)
public class Monument {

    @Id // @Id marks a field as a primary key field.
    @GeneratedValue // @GeneratedValue indique que la clé primaire est générée de façon automatique lors de l’insertion en base (et donc auto incrémenté aussi)
    private Integer id;

    private String nom;
    private String description;
    private String urlAudio;
    private String urlImage;
    private String categorie;



    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL) // @OneToMany => L'élément suivant est une référence à une autre table et peut contenir plusieurs instances de cet élément (toMany)
    private List<Point> pts;


    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlAudio() {
        return urlAudio;
    }

    public void setUrlAudio(String urlAudio) {
        this.urlAudio = urlAudio;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public List<Point> getPts() {
        return pts;
    }

    public void setPts(List<Point> pts) {
        this.pts = pts;
    }
}
