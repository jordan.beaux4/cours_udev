{var donneesRecup ;
var indexUl = 0;
    function afficher(d){

        var dataService = d.todoListes;
        
        for (var i = 0; i < dataService.length; i++){
            $("#contenu").append("<h3>"+ dataService[i]['name'] +"</h3>");
            $("#contenu").append("<ul id=" + indexUl+ "></ul>");

            var dataElement = dataService[i]['elements'];  
            
            for (var j = 0 ; j < dataElement.length; j++){

                $("#"+i).append("<li>" + dataElement[j]+ "</li>");
                
            }
        indexUl++;
        }  

        $("ul").after("<button onclick='ajTask()'> + </button>"); 
        $('li').append("<button onclick='suppTask()'> x </button>"); 
        $("#contenu").after("<button onclick='ajLi()'>Ajouter une liste </button>");
    }
    
    function ajTask(){

        var cible = event.target;
        var parentCible = $(cible).prev();       
        $(parentCible).append("<li><input type='text' placeholder='Votre nouvelle tâche'><button onclick='validerLi()'> Valider </button></li>");
    }

    function suppTask(){

        console.log("Test suppTask");
    }

    function ajLi(){

        $("#contenu").append("<h3><input type='text'><button onclick='validerTitre()'> Valider </button></h3>");
        $("#contenu>h3:last").after("<ul id="+ indexUl+ "></ul>");
        $("#"+indexUl).after("<button onclick='ajTask()'> + </button>");
        indexUl++;
    } 

    function validerTitre(){

        var cible = event.target;
        var parentCible = $(cible).prev().parent();
        var valInput = $(cible).prev()
        var valTitre = $(valInput).val();
        $(parentCible).text(valTitre);
        var objTi = {name:valTitre, elements:[]}
        donneesRecup.todoListes.push(objTi);

        envoyer(donneesRecup);
    }

    function validerLi(){

        var cible = event.target;
        var input = $(cible).prev();
        var valInput = $(input).val();
        var valLi =  $(cible).prev().parent();
        $(valLi).text(valInput);
        var valCompare = $(valLi).parent().prev().text(); // Récupérer le titre de la liste
        var valListe = $(valLi).text();
        for (var i =0;i<donneesRecup.todoListes.length;i++){
            
            if(donneesRecup.todoListes[i].name == valCompare){
    
                donneesRecup.todoListes[i].elements.push(valListe);
            }
        };
        console.log(donneesRecup);
        envoyer(donneesRecup);
    }

    function envoyer(donnees){

        $.ajax({
            type:'post',
            data: JSON.stringify(donnees),
            contentType: "application/json; charset=utf-8",
            url:"http://92.222.69.104:80/todo/listes/"

        }).done(function(){

            console.log("Datas sent");
        }).fail(function(){
            console.log("Error with transmission")
        });
    }

    function register(){

        var identifiant = $("#identifiant").val();
        var mdp = $("#mdp").val();
        $.ajax({
            url:"http://92.222.69.104:80/todo/create/" + identifiant +"/" + mdp
            }).done(function(d) {
                console.log(d);
                alert("Votre compte a bien été créé");
                $("#connexion").hide();
            });
        
    }

    function login (){

        var identifiantCo = $("#identifiantCo").val();
        var mdpCo = $("#mdpCo").val();
        $.ajax({
            url:"http://92.222.69.104:80/todo/listes/",
            headers: {
                'login' : identifiantCo,
                'password' : mdpCo,
            }
            }).done(function(d) {
                console.log(d);
                $("#connexion").hide();
                afficher(d);
                donneesRecup = d;
            });

    }

}