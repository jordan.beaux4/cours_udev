{var donneesRecup ; // variable qui récupère le JSON du login
var indexUl = 0; // variable pour numéroter le UL (son ID)
var indexLi = 0; // variable pour numéroter les LI

    function afficher(d){ // fonction qui affiche le JSON récupéré après le login
        $("#contenu").empty(); // Vidage de la div qui contient les listes pour le ré afficher "propre" à chaque modif
        var dataService = d.todoListes; // accès au todoListes => d est le JSON renvoyé par la fonction login (cf + bas) et qui est ici récupérer en paramètre de la fonction afficher
        // donc pour accéder à la partie de la todoListes, comme dans TOUS les JSON on fait "JSON.élément visé" (besoin de savoir comment est construit le JSON donc on le console.log avant)
        
        for (var i = 0; i < dataService.length; i++){ // boucle d'affichage de l'ul et des li du JSON
            $("#contenu").append("<h3>"+ dataService[i]['name'] +"</h3>"); // puisque le JSON contient un tableau, on le parcours avec une boucle et pour chaque passage, on écrit l'élément [name] du JSON (cf écriture dans la boucle)
            $("#contenu").append("<ul id=listeUl" + indexUl+ "></ul>"); // A la suite, on ajoute la UL que l'on remplira après. On lui ajoute un ID dynamique via indexUL (définit au départ) qu'on incrémente pour faciliter les manipulations + tard

            var dataElement = dataService[i]['elements']; // même principe que pour récupérer le nom, ici on récupère l'attribut [elements]
            
            for (var j = 0 ; j < dataElement.length; j++){ // On rebalaie le tableau d'éléments avec une boucle

                $("#listeUl"+indexUl).append("<li id=elemLi"+ indexLi + ">" + dataElement[j]+ "</li>"); // boucle d'affichage des LI via l'élément de l'index j (car les éléments du JSON sont ICI dans un tableau). Ajout d'un id dynamique (même principe que pour les UL)
                indexLi++; //incrémentation de l'id des li
                
            }
        indexUl++; // incrémentation de l'id des ul
        }  

        $("ul").after("<button onclick='ajTask()' class='ajouterLi'> + </button><button onclick='suppListe()' class='suppLi'> X </button>"); 
        $('li').append("<button onclick='suppTask()' class='effacer'> x  </button>"); 
      // Création des boutons d'ajout de li aux listes existantes (le +), de suppression de listes (le X) et boutons de suppressions à chaque élément (le x). affectation d'une classe pour CSS
    }
    
    function ajTask(){ // ajout d'une tache aux li

        var cible = event.target; // récupère l'élément qui a lancé la fonction (donc ici le bouton)
        var parentCible = $(cible).prev(); // prend l'élément avant le bouton (donc le ul dans notre DOM)       
        $(parentCible).append("<li id=elemLi" +indexLi + "><input type='text' placeholder='Votre nouvelle tâche'><button onclick='validerLi()' class='valider'> Valider </button></li>");
        indexLi++; // ajout d'une li sous forme d'input qui sera saisi par utilisateur et bouton valider dans le li ciblé (via le target et le prev)
    }

    function suppTask(){ // suppression d'une tache dans une liste

        var cible = event.target; // même principe, récupération de la cibe du bouton
        var idLi = $(cible).parent().attr('id'); // récupère l'id du parent du bouton cliqué.Le bouton étant dans le li (son parent est le li), on prend l'id du LI
        var index = $("#"+idLi).index(); // récupère l'index du li cliqué via son ID (récupéré juste avant)
        var prevCible = $(cible).parent().parent().prev(); // récupère l'élément précédent du parent du parent du li cliqué (ordre : bouton > li > ul> h3) pour avoir le titre de la liste qui servira à identifier dans le JSON où supprimer
        var titreUl = $(prevCible).text();  // récupère la valeur du h3 de la liste (text car en dur, si input, .val())     

        for(var i = 0; i<donneesRecup.todoListes.length;i++){ //boucle pour trouver l'élément a suppr en comparant avec le JSON donc on balaie le JSON comme précédemment

            if(donneesRecup.todoListes[i].name == titreUl){ // si le titre dans le JSON correspond avec celui récupéré

                donneesRecup.todoListes[i].elements.splice([index],1); // suppression du li supprimé du DOM (mis dans variable index) dans le JSON
            }

        }
        envoyer(donneesRecup); // A chaque fois que l'on modifie le DOM, on lance la fonction envoyer(JSON à envoyer) pour bien actualiser le JSON du webservice

        }

        function suppListe(){ //Suppression d'une liste complète (même principe que pour les li)

            var cible = event.target;
            var prevCible = $(cible).prev().prev().prev();
            var valTitre = $(prevCible).text();
        
            for(var i = 0; i<donneesRecup.todoListes.length;i++){

                if(donneesRecup.todoListes[i].name == valTitre){ //ici on veut supprimer toute une liste de taches donc besoin juste du h3 des ul que l'on compare avec notre JSON
                donneesRecup.todoListes.splice([i],1);
                }
            }

            envoyer(donneesRecup); // mise à jour du JSON
        }
        
        
    function ajLi(){ //Ajout d'une liste de taches

        $("#contenu").append("<h3><input type='text' placeholder='Votre nouvelle liste'><button onclick='validerTitre()' class='valider'> Valider </button></h3>"); // création de l'input de titre avec bouton valider à la suite dans notre div contenu
        $("#contenu>h3:last").after("<ul id=listeUl"+ indexUl+ "></ul>"); // création de l'ul après le titre que l'on remplira avec ajTask
        $("#listeUl"+indexUl).after("<button onclick='ajTask()' class='ajouterLi'> + </button>"); // ajout du bouton d'ajout de liste
        indexUl++; // puisqu'on crée une liste, ne pas oublier d'incrémenter le créateur d'ID
    } 

    function validerTitre(){ // Validation et envoi du titre au serveur

        var cible = event.target; // récupère la cible du bouton valider
        var parentCible = $(cible).prev().parent(); // Récupération de la balise h3 pour transformer input en donnée "dure"
        var valInput = $(cible).prev() // Sélection de l'input remplit
        var valTitre = $(valInput).val(); // récupère la valeur de l'input sélectionné
        $(parentCible).text(valTitre); // transforme la valeur de l'input en valeur en dure (on remplace le texte de l'input par ce qu'elle contenait en dure)


        var objTi = {name:valTitre, elements:[]} // création d'un JSON que l'on va envoyer => reprendre la STRUCTURE CONNUE
        donneesRecup.todoListes.push(objTi); // push de l'élément dans l'objet créé, à la racine car les titres sont stockés comme ça dans le JSON

        envoyer(donneesRecup); // Actualisation du JSON
    }

    function validerLi(){ // validation et envoi des taches (même principe mais besoin d'identifier où ajouter les li)

        var cible = event.target; // récupère le bouton
        var input = $(cible).prev(); // sélection de l'input (avant le bouton dans notre DOM)
        var valInput = $(input).val(); // récupération de la valeur de l'input saisi
        var valLi =  $(cible).prev().parent(); 
        $(valLi).text(valInput); // transforme la valeur de l'input en valeur en dure (on remplace le texte de l'input par ce qu'elle contenait en dure)
        var valCompare = $(valLi).parent().prev().text(); // Récupérer le titre de la liste
        var valListe = $(valLi).text(); // transforme la valeur de l'input en valeur en dure (on remplace le texte de l'input par ce qu'elle contenait en dure)
    
        for (var i =0;i<donneesRecup.todoListes.length;i++){ // boucle pour identifier où insérer le li dans le bon ul selon le titre
            
            if(donneesRecup.todoListes[i].name == valCompare){
    
                donneesRecup.todoListes[i].elements.push(valListe);
            }
        };
        envoyer(donneesRecup);
    }

    function envoyer(donnees){ // envoi des données au web service avec en paramètre le JSON modifié

        $.ajax({
            type:'post',
            data: JSON.stringify(donnees),
            contentType: "application/json; charset=utf-8",
            url:"http://92.222.69.104:80/todo/listes/"

        }).done(function(data){
            afficher(data); // A chaque fois qu'on envoie le JSON modifié, on ré affice en retour le JSON => cela permet d'avoir un affichage en temps des données de notre JSON
            console.log("Datas sent");
        }).fail(function(){
            console.log("Error with transmission")
        });
    }

    function register(){ //enregistrement d'un nouvel utilisateur

        var identifiant = $("#identifiant").val();
        var mdp = $("#mdp").val();
        $.ajax({
            url:"http://92.222.69.104:80/todo/create/" + identifiant +"/" + mdp
            }).done(function(d) {
                console.log(d);
                alert("Votre compte a bien été créé");
            });
        
    }

    function login (){ //connexion de l'utilisateur

        var identifiantCo = $("#identifiantCo").val(); // récupération des inputs saisis
        var mdpCo = $("#mdpCo").val();
        $.ajax({
            url:"http://92.222.69.104:80/todo/listes/",
            headers: {
                'login' : identifiantCo,
                'password' : mdpCo,
            }
            }).done(function(d) {
                console.log(d);
                $("#connexion").slideUp(250);
                afficher(d);
                $("#contenu").after("<button onclick='ajLi()' class='ajouterListe'>Ajouter une liste </button>") // ajout du bouton d'ajout de liste après l'affichage des listes
                donneesRecup = d; // transfère du JSON dans notre variable globale pour modification et utilisation dans tout le fichier
            });
    }
}