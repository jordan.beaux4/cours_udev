function pageAccueil(){
    displayMain();
    fillMain();
    fillNavbar();
}

function pageHistoire(){
    displayAsideMaine();
    fillMain();
    fillAside();
    fillNavbar();
}

function displayMain(){
    var tohtml='<div class="col-md-12 main"></div>';
    $(".container-fluid > .row").html(tohtml);
}

function displayAsideMaine(){
    var tohtml='<div class="col-md-2 aside"></div><div class="col-md-10 main"></div>';
    $(".container-fluid > .row").html(tohtml);
}

function fillMain(){
    var tohtml='<h1>main</h1>';
    $(".main").html(tohtml);
}

function fillAside(){
    var tohtml='<h1>aside</h1>';
    $(".aside").html(tohtml);
}

function fillNavbar(){
    var tohtml = ' <ul class="navbar-nav "><li class="nav-item"> <a class="nav-link" href="javascript:void(0)" onclick="" data-toggle="modal" data-target="#popUpWindow">Connection</a>  </li> <li class="nav-item"><a class="nav-link" href="javascript:void(0)" onclick="inscription()">Inscription</a>  </li></ul>'
  tohtml+='<div class="modal fade" id="popUpWindow">';
            tohtml+='<div class="modal-dialog">';
            tohtml+='<div class="modal-content">';
            /*tohtml+='<!-- header -->';
            tohtml+='<div class="modal-header">';
            tohtml+='<button type="button" class="close" data-dismiss="modal">&times;</button>';
            tohtml+='<h3 class="modal-title">Login Form</h3>';
            tohtml+='</div>';*/
            tohtml+='<!-- body -->';
            tohtml+='<div class="modal-header">';
            tohtml+='<form role="form">';
            tohtml+='<div class="form-group">';
            tohtml+='<input type="email" class="form-control" placeholder="Email"/>';
            tohtml+='<input type="password" class="form-control" placeholder="Mot de passe" />';
            tohtml+='</div>';
            tohtml+='</form>';
            tohtml+='</div>';
            tohtml+='<!-- footer -->';
            tohtml+='<div class="modal-footer">';
            tohtml+='<button class="btn btn-dark btn-block">Connection</button>';
            tohtml+='</div>';
            tohtml+='</div>';
            tohtml+='</div>';
            tohtml+='</div>';
            tohtml+='</div>';
    $(".navbar").html(tohtml);
}
