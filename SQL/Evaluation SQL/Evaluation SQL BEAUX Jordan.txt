1.1/ DDL

create table tarif (
tar_id number(4),
tar_datedeb date,
tar_datefin date,
tar_valeur number(7,2),
constraint PK_tar primary key(tar_id));

create table reglement (
reg_id number(4),
reg_date date,
reg_mnt number(7,2),
constraint PK_reg primary key (reg_id));

create table parametre (
par_lib varchar2 (3),
par_valeur number(7,2),
constraint PK_par primary key (par_lib));

create table zones (
zon_id number(4),
zon_lib varchar2 (5),
constraint PK_zones primary key (zon_id));

create table typemp (
typ_id number(4),
typ_lib varchar2(25),
constraint PK_typemp primary key (typ_id));

create table clients (
cli_id number(4),
cli_nom varchar2(50),
cli_pnom varchar2(30),
cli_adr varchar2(100),
cli_cp varchar2(10),
cli_ville varchar2(50),
cli_tel varchar2(15),
cli_mail varchar2(50),
constraint PK_clients primary key(cli_id));

create table emplacement(
emp_id number(4),
zon_id number(4),
typ_id number(4),
emp_elect number(1),
emp_ombre number(1),
emp_point number(7,2),
emp_parking number(1),
constraint PK_emp primary key (emp_id),
constraint FK_emp_type foreign key (typ_id) references typemp(typ_id),
constraint FK_emp_zone foreign key (zon_id) references zones(zon_id),
constraint CK_em_elect check (emp_elect in (0,1)),
constraint CK_em_ombre check (emp_ombre in (0,1)),
constraint CK_em_parking check (emp_parking in (0,1))
);

create table reservation(
res_id number(4),
emp_id number(4),
cli_id number(4),
res_datedeb date,
res_datefin date,
res_comptdeb number(7,2),
res_comptfin number (7,2),
constraint PK_res primary key (res_id),
constraint FK_res_emp foreign key (emp_id) references emplacement(emp_id),
constraint FK_res_cli foreign key (cli_id) references clients(cli_id)
);

create table lier (
res_id number(4),
reg_id number(4),
constraint PK_lier primary key (res_id,reg_id),
constraint FK_lier_res foreign key (res_id) references reservation(res_id),
constraint FK_lier_reg foreign key (reg_id) references reglement (reg_id)
);

create table activite (
act_id number(4),
zon_id number(4),
act_lib varchar2(50),
constraint PK_act primary key (act_id),
constraint FK_act_zones foreign key (zon_id) references zones(zon_id)
);

--------

1.2/ DML

insert into zones values (1,'zone1');
insert into zones values (2,'zone2');
insert into zones values (3,'zone3');
insert into zones values (4,'zone4');
insert into zones values (5,'zone5');

insert into activite values (1,1,'piscine');
insert into activite values (2,3,'tennis');
insert into activite values (3,4,'pétanque');
insert into activite values (4,4,'barbecue');

insert into typemp values (1,'vide');
insert into typemp values (2,'avec mobile home');
insert into typemp values (3,'avec caravane');

insert into emplacement values (
1,
(select zon_id
from activite
where act_lib ='tennis'),
(select typ_id
from typemp
where typ_lib = 'vide'),
1,
1,
25,
1);


insert into emplacement values (
2,
(select zon_id
from zones
where zon_lib ='zone5'),
(select typ_id
from typemp
where typ_lib = 'vide'),
1,
0,
20,
1);

1) Pour insérer un enregistrement dans la table RESERVATION, un client doit d'abord être créé dans la table client afin de lui attribuer un identifiant (cli_id).
Il doit aussi préciser son emplacement (car l'information est nécessaire pour créer la réservation via la foreign key de la table reservation) ainsi que des informations diverses qui seront traitées par la surcouche logicielle (son nom, prénom, adresse etc).

insert into clients values (
1,
'JOLI',
'Jordane',
'1 Rue ambroise paré',
'71380',
'SANT-MARCEL',
'06123456',
'exemple@exemple.fr');

Le client choisit un emplacement (vide, avec mobile home ou caravane), une option électricité (ou non), ombragé (ou non) et avec un parking (ou non). Il définit ses dates de séjour du 30/06/2020 au 07/07/2020 et on relève son compteur d'éléctricité. On relevera son compteur à son départ

insert into reservation values (
1,
id de l'emplacement selon les demandes du client (à l'ombre, sans électricité et parking par exemple),
id du client créé (1 ici),
to_date('30/06/20','DD/MM/YY'),
to_date('07/07/20','DD/MM/YY'),
20,
à renseigner au départ du client);

2) insert into clients values (
1,
'WAYNE',
'Bruce',
'1007 Mountain Drive',
'041939',
'Gotham City',
'06123456',
null);

3) update emplacement
set emp_point = (emp_point +2)
where typ_id <> 2;

OU DE MANIERE DYNAMIQUE

update emplacement
set emp_point = (emp_point +2)
where typ_id <> (select typ_id from typemp where typ_lib = 'avec mobile home');

--------

1.3/ Requêtes de sélection

1) select cli_nom, cli_pnom, cli_ville, cli_cp
from clients
where cli_cp like '69%';

2) select cli_nom, cli_pnom
from clients
where cli_mail is null;

3) select cli_nom, cli_pnom
from clients
join reservation
on clients.cli_id=reservation.cli_id
join emplacement
on emplacement.emp_id = reservation.emp_id
where emp_ombre = 1;

4) select emp_id
from emplacement
where emp_ombre = 1
and emp_elect = 1;

ERRATUM : dans ma création de table, j'ai mis emp_elect au lieu de emp_elec.

5) select emp_id
from emplacement
where typ_id = (select typ_id from typemp where typ_lib = 'vide');

6) select emp_id
from emplacement
where zon_id = 1;

OU DYNAMIQUEMENT

select emp_id
from emplacement
where zon_id = (select zon_id from activite where act_lib='piscine');

7) select emp_id
from reservation
where res_datedeb between to_date('01/07/2020','DD/MM/YYYY') and to_date ('31/07/2020','DD/MM/YYYY')
and res_datefin between to_date('02/07/2020','DD/MM/YYYY') and to_date ('31/07/2020','DD/MM/YYYY')
and res_id <> null;

La requête renvoie le ou les numéros d'emplacement après avoir vérifié si un numéro de réservation existe (donc que des réservations ont été créées) et si la date de début de réservation est entre le 1 juillet 2020 et le 31 juillet 2020 (pareil pour la date de fin avec le 2 juillet car séjour d'au moins un jour). 
S'il y a correspondance, alors que c'est que l'emplacement est attribué à une réservation et que donc il est réservé.



