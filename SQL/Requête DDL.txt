create table producteurs
(nop number(5), nom varchar2 (20) not null, region varchar2 (20), constraint PK_prod PRIMARY KEY (nop),CONSTRAINT CK_region check (region in ('Lowland', 'Highland', 'Speyside', 'Islay', 'Campbeltown')));


create table whisky (
nwhisky number (5),
typewhisky varchar2(20),
degre number(3,1),
constraint PK_whisky primary key (nwhisky), 
constraint CK_type check (typewhisky in ('single malt','blended', 'single barrel', 'blended malt', 'blended grain')),
constraint CK_degre check (degre between '38' and '63')
);


create table buveurs(
nob number(5),
nom varchar2 (20) not null,
prenom varchar2(20),
constraint PK_nob primary key (nob));


create table achats(
nwhisky number(5),
nob number(5),
quantite number(5) not null,
constraint PK_nwhisky primary key (nwhisky,nob),
constraint FK_achat_nwhisky foreign key (nwhisky) references whisky(nwhisky),
constraint FK_buveur_achat foreign key (nob) references buveurs(nob)
);


create table production (
nop number(5),
nwhisky number(5),
annee number(4) not null,
quantite number(5) not null,
constraint PK_nop_nwhisky primary key (nop,nwhisky),
constraint FK_nop foreign key (nop) references producteurs(nop),
constraint FK_nwhisky foreign key (nwhisky) references whisky(nwhisky)
);

insert into producteurs values (1, 'Bladnoch', 'Lowland');
insert into producteurs values (2, 'Glenkinchie', 'Lowland');
insert into producteurs values (3, 'Auchentoshan', 'Lowland');
insert into producteurs values (4, 'Highland Park', 'Highland');
insert into producteurs values (5, 'Fettercairn', 'Highland');
insert into producteurs values (6, 'Dewar', 'Highland');
insert into producteurs values (7, 'Aberlour', 'Speyside');
insert into producteurs values (8, 'Macallan', 'Speyside');
insert into producteurs values (9, 'Glenlivet', 'Speyside');
insert into producteurs values (10, 'Caol Ila', 'Islay');
insert into producteurs values (11, 'Lagavulin', 'Islay');
insert into producteurs values (12,'Laphroaig', 'Islay');
insert into producteurs values (13, 'Loch Lomond', 'Campbeltown');

insert into whisky values (1, 'single barrel', 55);
insert into whisky values (2, 'single malt', 50);
insert into whisky values (3, 'blended grain', 56.5);
insert into whisky values (4, 'blended malt', 46);
insert into whisky values (5, 'single barrel', 51);
insert into whisky values (6, 'single malt', 49);
insert into whisky values (7, 'blended malt', 40);
insert into whisky values (8, 'blended grain', 58);

insert into production values (11, 1, 1977, 100);
insert into production values (11, 2, 1977, 80);
insert into production values (7, 3, 1977, 20);
insert into production values (13, 4, 1977, 30);
insert into production values (13, 2, 1978, 8);
insert into production values (6, 5, 1971, 10);
insert into production values (2, 6, 1972, 12);
insert into production values (2, 2, 1977, 7);
insert into production values (1, 6, 1941, 12);
insert into production values (10, 7, 1952, 71);


insert into buveurs values (1, 'Haddock', null);
insert into buveurs values (2, 'Tintin', null);
insert into buveurs values (3, 'Churchill','Winston');
insert into buveurs values (4, 'G', 'G');
insert into buveurs values (5, 'Mito', null);

insert into achats values (
(select nwhisky
from whisky
where typewhisky ='single malt' and degre = 49), 
(select nob from buveurs where nom ='Haddock'),
10);

insert into achats values (
(select nwhisky
from whisky
where typewhisky ='blended malt' and degre = 46), 
(select nob from buveurs where nom ='Haddock'),
20);

insert into achats values (
(select nwhisky
from whisky
where typewhisky ='blended malt' and degre = 40), 
(select nob from buveurs where nom ='Churchill'),
2);

insert into achats values (
(select nwhisky
from whisky
where typewhisky ='single malt' and degre = 50), 
(select nob from buveurs where nom ='G'),
10);

insert into achats values (
(select nwhisky
from whisky
where typewhisky ='blended grain' and degre = 58), 
(select nob from buveurs where nom ='Mito'),
15);

------

select distinct whisky.typewhisky, whisky.degre, production.annee
from whisky
join production
on whisky.nwhisky = production.nwhisky
order by whisky.typewhisky asc, production.annee desc ;

