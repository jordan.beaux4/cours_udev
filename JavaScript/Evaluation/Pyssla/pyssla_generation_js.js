// Méthode avec Zone de couleur

function takeColor(){

    var element = event.target;
    var myColor = element.getAttribute('class');
    var colorSaved = document.getElementById('color');
    var classColorSaved = colorSaved.getAttribute('class');


    if(classColorSaved!=null){

        colorSaved.classList.remove(classColorSaved);
        colorSaved.classList.add(myColor);

    }
    else colorSaved.classList.add(myColor);   

}

function applyColor(){
    
    var whereApplied = event.target;
    var myActualClass = whereApplied.getAttribute('class');
    var newcolorSaved = document.getElementById('color');
    var newColor = newcolorSaved.getAttribute('class');

    if(myActualClass!=null){

        whereApplied.classList.remove(myActualClass);
        whereApplied.classList.add(newColor);

    }
    else whereApplied.classList.add(newColor);   

}

function Tableau(){

    // PRE-REQUIS 1 : L'utilisateur ne rentre pas de donnée négative
    // PRE-REQUIS 2 : L'utilisateur ne re génère pas de grille avant d'avoir effacer le suivant

    var hauteur = document.getElementById('hauteur').value; 
    var largeur = document.getElementById('largeur').value;
  
    var creaTab = document.getElementById('contenant');
    creaTab.innerHTML += "<table border='3' id='pyssla' cellpadding='20' cellspacing='5'></table>";
    var pyssla = document.getElementById('pyssla');

    for (var i = 0; i < hauteur;i++){

        pyssla.innerHTML += "<tr class='lignes'></tr>";
        
    }

    var lignes = document.getElementsByClassName('lignes');

        for (var j = 0; j < lignes.length;j++){
            
            var k = 0;

            while(k < largeur){

                lignes[j].innerHTML += "<td onclick='applyColor()'></td>";
                k++;

            }
        }  
}

function Effacer(){

    // Action : efface TOUTE la grille

    var delPyssla = document.getElementById('pyssla');

    delPyssla.innerHTML ="";

}

function reset(){

    // Action : efface les couleurs de la grille

    var cellule = document.getElementsByTagName("td");

    for (var l = 0; l < cellule.length;l++){
    cellule[l].removeAttribute("class");
    }
}



