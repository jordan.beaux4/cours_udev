// Methode avec modification en brut de la couleur => non fonctionnelle

/* function takeColor(){

    var element = event.target;
    var myColor = getComputedStyle(element); element.getAttribute('class')
    var myBGcolor = myColor.backgroundColor;
    return myBGcolor;
}

function applyColor(){

    var whereApplied = event.target;
    var colorToApplied = takeColor();
    var myNColor = getComputedStyle(whereApplied);
    myNColor.backgroundColor = colorToApplied;
    
} */

// Méthode avec Zone de couleur

function takeColor(){

    var element = event.target;
    var myColor = element.getAttribute('class');
    var colorSaved = document.getElementById('color');
    var classColorSaved = colorSaved.getAttribute('class');


    if(classColorSaved!=null){

        colorSaved.classList.remove(classColorSaved);
        colorSaved.classList.add(myColor);

    }
    else colorSaved.classList.add(myColor);   

}

function applyColor(){
    
    var whereApplied = event.target;
    var myActualClass = whereApplied.getAttribute('class');
    var newcolorSaved = document.getElementById('color');
    var newColor = newcolorSaved.getAttribute('class');

    if(myActualClass!=null){

        whereApplied.classList.remove(myActualClass);
        whereApplied.classList.add(newColor);

    }
    else whereApplied.classList.add(newColor);   

}



